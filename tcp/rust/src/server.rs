use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};

fn handle_client(mut stream: TcpStream) {
    let mut buf= [0; 128];
    if let Ok(len) = stream.read(&mut buf) {
        println!("Client: {}", String::from_utf8(buf[0..len].to_vec()).unwrap());
    }

    let msg = "Hellow from server";
    stream.write( msg.as_ref());
}

fn main() -> std::io::Result<()> {
    let mut listener = TcpListener::bind("127.0.0.1:50080")?;

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => handle_client(stream),
            Err(e) => eprintln!("{}", e),
        }
    }
    Ok(())
}
