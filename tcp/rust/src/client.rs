use std::io::{Read, Write};
use std::net::TcpStream;

fn main() -> std::io::Result<()> {
    let mut stream = TcpStream::connect("127.0.0.1:50080")?;

    let msg = "Hello from client";
    stream.write(msg.as_ref())?;

    let mut buf = [0; 128];
    if let Ok(len) = stream.read(&mut buf) {
        println!("Server: {}", String::from_utf8(buf[0..len].to_vec()).unwrap());
    }

    Ok(())
}