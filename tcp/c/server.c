
#include <netinet/in.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 50080
#define MSG_LEN 512

int main() {
	int sock_fd, newsock_fd;
	int opt = 1;
	socklen_t addrlen;
	struct sockaddr_in srv_addr, cli_addr;
	char cli_msg[MSG_LEN] = {0};

	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_fd < 0) {
		perror("Failed to open socket");
		return 1;
	}
	setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));

	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = INADDR_ANY;
	srv_addr.sin_port = htons(PORT);
	if (bind(sock_fd, (struct sockaddr *) &srv_addr, sizeof(srv_addr)) < 0) {
		perror("Failed to bind socket");
		close(sock_fd);
		return 1;
	}

	addrlen = sizeof(cli_addr);
	listen(sock_fd, 5);
	newsock_fd = accept(sock_fd, (struct sockaddr *) &cli_addr, &addrlen);
	if (newsock_fd < 0) {
		perror("Failed to accept client connection");
		close(sock_fd);
		return 1;
	}

	read(newsock_fd, cli_msg, MSG_LEN);
	printf("Client: %s\n", cli_msg);

	write(newsock_fd, "Hi", 2);

	close(newsock_fd);
	close(sock_fd);
	return 0;
}