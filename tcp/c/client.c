
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>

#define ADDR "127.0.0.1"
#define PORT 50080
#define MSG_LEN 512

int main() {
	int sock_fd, n;
	struct sockaddr_in srv_addr;
	struct hostent *server;
	char srv_msg[MSG_LEN] = {0};

	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_fd < 0) {
		perror("Failed to open socket");
		return 1;
	}

	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(PORT);

	if (inet_pton(AF_INET, ADDR, &srv_addr.sin_addr) < 0) {
		perror("Invalid/Unsupported address");
		return 1;
	}

	if (connect(sock_fd, (struct sockaddr *) &srv_addr, sizeof(srv_addr)) < 0) {
		perror("Failed to connect");
		return 1;
	}

	write(sock_fd, "Hello", 5);

	read(sock_fd, srv_msg, MSG_LEN);
	printf("Server: %s\n", srv_msg);

	close(sock_fd);
	return 0;
}